import { Component, OnInit } from '@angular/core'
import { Contact } from '../models/contact.model'
import { ContactsService } from '../services/contacts.service'
import { SelectedContactService } from '../services/selected-contact.service';

@Component({
    selector:'app-contact-list',
    templateUrl: './contact-list.component.html',
    styleUrls: ['./contact-list.component.css']
})

export class ContactListComponent implements OnInit{
    constructor(private readonly contactService: ContactsService,
                private readonly selectedContactService: SelectedContactService)
                {

    }
    ngOnInit():void{
        this.contactService.fetchContacts();
    }
    get contacts(): Contact[] {
        return this.contactService.contacts();
    }
    public handleContactClicked(contact: Contact): void{
        this.selectedContactService.setContact(contact);
    }
}
